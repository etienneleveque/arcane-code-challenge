# Arcane Code Challenge

Web application for real estate management.

## Install

```bash
# clone
git clone https://gitlab.com/etienneleveque/arcane-code-challenge.git
cd arcane-code-challenge

# install with pipenv
pipenv install --skip-lock
pipenv shell

export FLASK_APP=run.py
export FLASK_ENV=development
export APP_SETTINGS=development

flask db upgrade

flask run
```

## Run test

```bash
python test_arcane_code_challenge.py
```

## API endpoints

ALL endpoint with `api/users/` need authentification, except for POST method.

This application uses http basic auth for authentification.

| HTTP Method | **URL**                                                | Description                                      |
| ----------- | ------------------------------------------------------ | ------------------------------------------------ |
| POST        | `/tokens`                                              | Return token.                                    |
| GET         | `/users`                                               | (Admin only) Return the collection of all users. |
| POST        | `/users`                                               | Register a new user account.                     |
| DELETE      | `/users`                                               | (Admin only) Delete all users. (To test project) |
| GET         | `/users/<id>`                                          | Return a user.                                   |
| PUT         | `/users/<id>`                                          | Edit a user.                                     |
| DELETE      | `/users/<id>`                                          | Delete a user.                                   |
| GET         | `/users/<id>/properties`                               | Return the collection of properties for a user.  |
| POST        | `/users/<id>/properties`                               | Register a new property for a user.              |
| GET         | `/users/<id>/properties/<property_id>`                 | Return a property.                               |
| PUT         | `/users/<id>/properties/<property_id>`                 | Edit a property                                  |
| DELETE      | `/users/<id>/properties/<property_id>`                 | Delete a property.                               |
| GET         | `/users/<id>/properties/<property_id>/rooms`           | Return the collection of rooms for a property.   |
| POST        | `/users/<id>/properties/<property_id>/rooms`           | Register a new room for a property.              |
| GET         | `/users/<id>/properties/<property_id>/rooms/<room_id>` | Return a room.                                   |
| PUT         | `/users/<id>/properties/<property_id>/rooms/<room_id>` | Edit a room.                                     |
| DELETE      | `/users/<id>/properties/<property_id>/rooms/<room_id>` | Delete a room.                                   |
| GET         | `/properties`                                          | Return the collection of properties.             |
| GET         | `/properties/<property_id>`                            | Return a property.                               |
| GET         | `/properties/<property_id>/rooms`                      | Return the collection of rooms for a property.   |
| GET         | `/properties/<property_id>/rooms/<room_id>`            | Return a room.                                   |

## Example

```bash
# Create User
http http://localhost:5000/api/users name=suzan last_name=robert email=suzan@example.com password=azerty

# Get token
http --auth "suzan@example.com":"azerty" POST http://localhost:5000/api/tokens
# {"token": "XrMJR6hnTIDaRrAwsOEXtc/r2bD/Gdyl"}

# Create property
http http://localhost:5000/api/users/1/properties "Authorization:Bearer XrMJR6hnTIDaRrAwsOEXtc/r2bD/Gdyl" name="T2" category="studio" city="Paris"

# Search property
http http://localhost:5000/api/properties?city=Paris
```
