from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

import os
import datetime as dt

from config import app_config

db = SQLAlchemy()
migrate = Migrate()


def create_app(config_name):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config.from_object(app_config[config_name])
    app.config["SQLALCHEMY_DATABASE_URI"] = os.path.join(
        "sqlite:///" + app.instance_path, app.config["SQLALCHEMY_DATABASE_NAME"]
    )

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    db.init_app(app)
    migrate.init_app(app, db)

    from app.api import bp

    app.register_blueprint(bp, url_prefix="/api")

    return app

