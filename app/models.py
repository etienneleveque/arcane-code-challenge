from werkzeug.security import generate_password_hash, check_password_hash
import base64
from datetime import datetime, timedelta
import os

from app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    last_name = db.Column(db.String(80), nullable=False)
    birthdate = db.Column(db.Date())
    email = db.Column(db.String(120), unique=True)
    password_hash = db.Column(db.String(128))
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    admin = db.Column(db.Boolean, default=False)
    properties = db.relationship("Property", backref="owner", lazy=True, cascade="delete")

    def __repr__(self):
        return "<User %r>" % self.email

    def from_dict(self, data, new_user=False):
        for field in ['name', 'last_name', 'birthdate', "email"]:
            if field in data:
                if field == 'birthdate':
                    setattr(self, field, datetime.strptime(data["birthdate"], "%Y-%m-%d"))
                else:
                    setattr(self, field, data[field])
        if new_user and 'password' in data:
            self.set_password(data['password'])

    def to_dict(self):
        data = {
            "id": self.id,
            "name": self.name,
            "last_name": self.last_name,
            "birthdate": self.birthdate,
            "email": self.email,
            "properties": [prop.to_dict() for prop in self.properties]
        }
        if self.admin:
            data["admin"] = True
        return data

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        db.session.commit()
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    def save(self):
        db.session.add(self)
        db.session.commit()


    @staticmethod
    def get_all():
        return User.query.all()

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class Property(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    name = db.Column(db.String(120), nullable=False)
    description = db.Column(db.String())
    category = db.Column(db.String(80), nullable=False)
    city = db.Column(db.String(120), nullable=False)
    rooms = db.relationship("Room", backref="property", lazy=True, cascade="delete")

    def __repr__(self):
        return "<Property %r>" % self.name

    def save(self):
        db.session.add(self)
        db.session.commit()

    def from_dict(self, data):
        for field in ['name', 'description', 'category', "city"]:
            if field in data:
                setattr(self, field, data[field])

    def to_dict(self):
        return dict(
            id=self.id,
            owner_id=self.owner_id,
            name=self.name,
            description=self.description,
            category=self.category,
            city=self.city,
            rooms=[room.to_dict() for room in self.rooms]
        )

    @staticmethod
    def get_all():
        return Property.query.all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class Room(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    property_id = db.Column(db.Integer, db.ForeignKey("property.id"), nullable=False)
    name = db.Column(db.String(120), nullable=False)
    description = db.Column(db.String(), nullable=False)

    def __repr__(self):
        return "<Room %r>" % self.name

    def save(self):
        db.session.add(self)
        db.session.commit()

    def from_dict(self, data):
        for field in ['name', 'description']:
            if field in data:
                setattr(self, field, data[field])

    def to_dict(self):
        return dict(
            id=self.id,
            property_id=self.property_id,
            name=self.name,
            description=self.description,
        )

    @staticmethod
    def get_all():
        return Room.query.all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()
