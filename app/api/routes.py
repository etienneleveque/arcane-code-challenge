from flask import Flask, request, jsonify, abort, g

from app import db
from app.models import User, Property, Room
from app.api import errors, tokens
from app.api import bp
from app.api.errors import bad_request
from app.api.auth import token_auth


@bp.route("/")
def hello_world():
    return "Hello, World!"


@bp.route("/users", methods=["GET", "DELETE"])
@token_auth.login_required
def users():
    if not g.current_user.admin:
        abort(403)
    if request.method == "GET":
        response = jsonify([user.to_dict() for user in User.get_all()])
        response.status_code = 200
        return response
    else:
        for user in User.get_all():
            user.delete()
        response = jsonify("All users deleted succesfully")
        response.status_code = 200
        return response


@bp.route("/users", methods=["POST"])
def register_user():
    data = request.get_json()
    if 'name' not in data or 'last_name' not in data or 'email' not in data or 'password' not in data:
        return bad_request('must include name, lastname, birthdate, email and password fields')
    if User.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    user = User()
    user.from_dict(data, new_user=True)
    user.save()
    response = jsonify(user.to_dict())
    response.status_code = 201
    return response


@bp.route("/users/<int:id>", methods=["GET", "PUT", "DELETE"])
@token_auth.login_required
def users_manipulation(id):
    if g.current_user.id != id:
        abort(403)

    user = User.query.get_or_404(id)

    if request.method == "DELETE":
        user.delete()
        return jsonify({"message": "user {} deleted successfully".format(user.id)})

    elif request.method == "PUT":
        data = request.get_json() or {}
        if 'email' in data and data['email'] != user.email and User.query.filter_by(email=data['email']).first():
            return bad_request('please use a different email address')
        user.from_dict(data, new_user=False)
        db.session.commit()
        response = jsonify(user.to_dict())
        return response

    else:
        return jsonify(user.to_dict())


@bp.route("/users/<int:id>/properties", methods=["GET", "POST"])
@token_auth.login_required
def get_user_properties(id):
    if g.current_user.id != id:
        abort(403)

    owner = User.query.get_or_404(id)

    if request.method == "POST":
        data = request.get_json()
        if 'name' not in data or 'category' not in data or 'city' not in data:
            return bad_request('must include name, description, category and city fields')
        owner_property = Property()
        owner_property.owner = owner
        owner_property.from_dict(data)
        owner_property.save()
        response = jsonify(owner_property.to_dict())
        response.status_code = 201
        return response

    else:
        response = jsonify([owner_property.to_dict() for owner_property in owner.properties])
        response.status_code = 200
        return response


@bp.route("/users/<int:id>/properties/<int:property_id>", methods=["GET", "PUT", "DELETE"])
@token_auth.login_required
def user_properties_manipulation(id, property_id):
    if g.current_user.id != id:
        abort(403)

    owner_property = Property.query.get_or_404(property_id)

    if owner_property.owner_id != id:
        abort(403)

    if request.method == "DELETE":
        owner_property.delete()
        return jsonify({"message": "property {} deleted successfully".format(owner_property.id)})

    elif request.method == "PUT":
        data = request.get_json() or {}
        owner_property.from_dict(data)
        db.session.commit()
        response = jsonify(owner_property.to_dict())
        return response

    else:
        return jsonify(owner_property.to_dict())


@bp.route("/users/<int:id>/properties/<int:property_id>/rooms", methods=["GET", "POST"])
@token_auth.login_required
def get_user_property_rooms(id, property_id):
    if g.current_user.id != id:
        abort(403)

    owner_property = Property.query.get_or_404(property_id)

    if owner_property.owner_id != id:
        abort(403)
    
    if request.method == "POST":
        data = request.get_json()
        if 'name' not in data or 'description' not in data:
            return bad_request('must include name and description fields')
        room = Room()
        room.property = owner_property
        room.from_dict(data)
        room.save()
        response = jsonify(room.to_dict())
        response.status_code = 201
        return response
    else:
        response = jsonify([room.to_dict() for room in owner_property.rooms])
        response.status_code = 200
        return response


@bp.route("/users/<int:id>/properties/<int:property_id>/rooms/<int:room_id>", methods=["GET", "PUT", "DELETE"])
@token_auth.login_required
def user_property_room_manipulation(id, property_id, room_id):
    if g.current_user.id != id:
        abort(403)

    owner_property = Property.query.get_or_404(property_id)

    if owner_property.owner_id != id:
        abort(403)

    room = Room.query.get_or_404(room_id)

    if request.method == "DELETE":
        room.delete()
        return jsonify({"message": "room {} deleted successfully".format(room.id)})

    elif request.method == "PUT":
        data = request.get_json() or {}
        room.from_dict(data)
        db.session.commit()
        response = jsonify(room.to_dict())
        return response

    else:
        response = jsonify(room.to_dict())
        response.status_code = 200
        return response



@bp.route("/properties", methods=["GET"])
def get_properties():
    if "city" in request.args.keys():
        response = jsonify([owner_property.to_dict() for owner_property in Property.query.filter_by(city=request.args.get("city")).all()])
        response.status_code = 200
        return response
    else:
        response = jsonify([owner_property.to_dict() for owner_property in Property.get_all()])
        response.status_code = 200
        return response


@bp.route("/properties/<int:property_id>", methods=["GET"])
def get_property(property_id):
    owner_property = Property.query.get_or_404(property_id)
    response = jsonify(owner_property.to_dict())
    response.status_code = 200
    return response


@bp.route("/properties/<int:property_id>/rooms", methods=["GET"])
def get_property_rooms(property_id):
    owner_property = Property.query.get_or_404(property_id)

    response = jsonify([room.to_dict() for room in owner_property.rooms])
    response.status_code = 200
    return response


@bp.route("/properties/<int:property_id>/rooms/<int:room_id>", methods=["GET"])
def get_property_room(property_id, room_id):
    room = Property.query.get_or_404(room_id)

    response = jsonify(room.to_dict())
    response.status_code = 200
    return response