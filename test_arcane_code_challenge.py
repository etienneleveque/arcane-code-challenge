from datetime import datetime, timedelta, date
import unittest
from werkzeug.datastructures import Headers
import base64
import json

from app import create_app, db
from app.models import User, Property, Room


class UserModelCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app("testing")
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client

        self.user = {
            "name": "susan",
            "last_name": "robert",
            "email": "susan.robert@example.com",
            "password": "secret",
        }

        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_password_hashing(self):
        u = User(name="susan", last_name="robert", email="susan.robert@example.com")
        u.set_password("cat")
        self.assertFalse(u.check_password("dog"))
        self.assertTrue(u.check_password("cat"))

    def test_create_user(self):
        # Create user
        rv1 = self.client().post("/api/users", json=self.user)
        self.assertEqual(rv1.status_code, 201)

        # Create user with same email
        rv2 = self.client().post(
            "/api/users",
            json={
                "name": "test",
                "last_name": "test",
                "email": "susan.robert@example.com",
                "password": "secret",
            },
        )
        self.assertEqual(rv2.status_code, 400)

        # Create user with password missing
        rv3 = self.client().post(
            "/api/users",
            json={
                "name": "susan",
                "last_name": "robert",
                "email": "susan2.robert@example.com",
            },
        )
        self.assertEqual(rv3.status_code, 400)


    def test_properties_management(self):
        # Create user
        rv1 = self.client().post("/api/users", json=self.user)
        id = json.loads(rv1.data.decode("utf-8"))["id"]
        self.assertEqual(rv1.status_code, 201)
        self.assertEqual(id, 1)

        # Get token for this user
        auth_header = Headers()
        auth_header.add(
            "Authorization",
            "Basic "
            + base64.b64encode(b"susan.robert@example.com:secret").decode("utf-8"),
        )
        rv2 = self.client().post("/api/tokens", headers=auth_header)
        token = json.loads(rv2.data.decode("utf-8"))["token"]
        self.assertEqual(rv2.status_code, 200)
        token_header = Headers()
        token_header.add("Authorization", "Bearer " + token)

        # Get user data
        rv3 = self.client().get(f"/api/users/{id}", headers=token_header)
        self.assertEqual(rv3.status_code, 200)

        # Get user data without auth
        rv4 = self.client().get(f"/api/users/{id}")
        self.assertEqual(rv4.status_code, 401)

        # Edit user data
        rv7 = self.client().put(f"/api/users/{id}",
            json={"birthdate": "2018-01-01"},
            headers=token_header
        )
        self.assertEqual(rv7.status_code, 200)

        # Get user data without auth
        rv4 = self.client().get(f"/api/users/{id}")
        self.assertEqual(rv4.status_code, 401)

        # create property
        rv5 = self.client().post(
            f"/api/users/{id}/properties",
            json={"name": "T2", "category": "studio", "city": "Paris"},
            headers=token_header,
        )
        property_data = json.loads(rv5.data.decode("utf-8"))
        name = property_data["name"]
        property_id = property_data["id"]
        self.assertEqual(rv5.status_code, 201)
        self.assertEqual(name, "T2")

        # create room for this property
        rv6 = self.client().post(
            f"/api/users/{id}/properties/{property_id}/rooms",
            json={"name": "Chambre", "description": "Petite chambre"},
            headers=token_header,
        )
        self.assertEqual(rv6.status_code, 201)

        # Test search by city
        rv7 = self.client().get("/api/properties?city=Paris")
        property_city = json.loads(rv7.data.decode("utf-8"))[0]["city"]
        self.assertEqual(property_city, "Paris")


if __name__ == "__main__":
    unittest.main(verbosity=2)
